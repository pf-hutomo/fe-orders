# pf-order
assumptions:
- nodejs already installed (in this project using version 20++)

## Project setup
```
run on the terminal: npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

notes:
consider to use process manager like pm2 or docker to keep the process running
